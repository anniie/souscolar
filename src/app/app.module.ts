import { environment } from '../environments/environment.prod';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';

import { AngularFireModule } from '@angular/fire';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { Facebook } from '@ionic-native/facebook/ngx';

import { IonicRatingModule } from 'ionic4-rating';
import {GooglePlus } from '@ionic-native/google-plus/ngx'
import {Storage} from '@ionic/storage';
import { JwtModule, JWT_OPTIONS } from '@auth0/angular-jwt';
import { AuthGuardService } from './services/auth-guard.service';
import { HomeComponent } from './home/home.component';


export function jwtOptionsFactory(storage) {
  return {
      tokenGetter: () => {
          return storage.get('access_token');
      },
      whitelistedDomains: ['192.168.202.44:8888']
  };
}

// export const firebase={
//   apiKey: "AIzaSyCo-rq7C-li_18NXsnaSloxrtUia1nzrHk",
//   authDomain: "souscolar.firebaseapp.com",
//   databaseURL: "https://souscolar.firebaseio.com",
//   projectId: "souscolar",
//   storageBucket: "souscolar.appspot.com",
//   messagingSenderId: "382401743066",
//   appId: "1:382401743066:web:218a0583efe1e1d898fde3"
// };

@NgModule({
  declarations: [AppComponent, HomeComponent],
  entryComponents: [],
  imports: [BrowserModule,
    IonicModule.forRoot(), 
    AppRoutingModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    AngularFirestoreModule,
    IonicRatingModule,
    JwtModule.forRoot({
      jwtOptionsProvider: {
          provide: JWT_OPTIONS,
          useFactory: jwtOptionsFactory,
          deps: [Storage],
      }
  }),],
  providers: [
    StatusBar,
    SplashScreen,
    GooglePlus,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    Storage,
    AngularFirestoreModule,
    Facebook,
    AuthGuardService
    
  ],
  bootstrap: [AppComponent]
})
export class AppModule {

}

