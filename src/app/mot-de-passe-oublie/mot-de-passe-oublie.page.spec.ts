import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { MotDePasseOubliePage } from './mot-de-passe-oublie.page';

describe('MotDePasseOubliePage', () => {
  let component: MotDePasseOubliePage;
  let fixture: ComponentFixture<MotDePasseOubliePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MotDePasseOubliePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(MotDePasseOubliePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
