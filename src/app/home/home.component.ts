import { Component, OnInit } from '@angular/core';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {

  navigate  =
  [
    {
      title : "Home",
      url   : "/choix-cours",
      icon  : "home"
    },
    {
      title : "Mes cours",
      url   : "/my-cours",
      icon  : "contacts"
    },
   
  ];
  selectedPath: any;

  constructor(
    public authService: AuthService,
    private statusBar: StatusBar
  ) { 
    
    this.statusBar.styleDefault();
  }

  ngOnInit() {}

  
  logOut(){
    this.authService.signOut()
  }

  test(){
    console.log("ca passe")
  }

}
