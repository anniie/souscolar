import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from './services/auth-guard.service';
import { HomeComponent } from './home/home.component';

const routes: Routes = [
  { path: '', redirectTo: '/connexion', pathMatch: 'full' },

  {
    path: 'connexion',
    loadChildren: () => import('./connexion/connexion.module').then( m => m.ConnexionPageModule)
  },

  {
    path: 'inscription',
    loadChildren: () => import('./inscription/inscription.module').then( m => m.InscriptionPageModule)
  },

  { 
    path: '', component: HomeComponent,
    children: [
      {
        path: 'choix-cours',
        loadChildren: () => import('./choix-cours/choix-cours.module').then( m => m.ChoixCoursPageModule),
        canActivate: [AuthGuardService]
      },
      {
        path: 'verify-email',
        loadChildren: () => import('./verify-email/verify-email.module').then( m => m.VerifyEmailPageModule)
      },
     
      {
        path: 'index',
        loadChildren: () => import('./index/index.module').then( m => m.IndexPageModule),
        canActivate: [AuthGuardService]
      },
      
      {
        path: 'my-cours',
        loadChildren: () => import('./my-cours/my-cours.module').then( m => m.MyCoursPageModule),
        canActivate: [AuthGuardService]
      },
    
      {
        path: 'details-matiere/:libelle',
        loadChildren: () => import('./details-matiere/details-matiere.module').then( m => m.DetailsMatierePageModule),
        canActivate: [AuthGuardService]
      },
    ],
    canActivate: [AuthGuardService]
  },
  {
    path: 'mot-de-passe-oublie',
    loadChildren: () => import('./mot-de-passe-oublie/mot-de-passe-oublie.module').then( m => m.MotDePasseOubliePageModule)
  },

  
];

@NgModule({
  imports : [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
