// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: true,
   firebase : {
        apiKey: "AIzaSyCNiH87vO_W059A2wDp_1cDy5MgjLO3CyM",
        authDomain: "souscolartest-204f5.firebaseapp.com",
        databaseURL: "https://souscolartest-204f5.firebaseio.com",
        projectId: "souscolartest-204f5",
        storageBucket: "souscolartest-204f5.appspot.com",
        messagingSenderId: "141911003171",
        appId: "1:141911003171:web:637afe9ab6d6e9a439e196"
      }  
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
