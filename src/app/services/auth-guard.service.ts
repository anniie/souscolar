import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService implements CanActivate {

  constructor(public auth: AuthService, public router: Router) {}

    // guard function
    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
      const isAuth = this.auth.isAuthenticated()
      if(isAuth){
        return isAuth;
      }
      this.router.navigate(['/connexion'], {queryParams: {returnUrl: state.url}}).then(r => r);
      return false;
    }
    
}
