import * as firebase from 'firebase/app';
import { Component, OnInit } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/database';
import {Validators, FormBuilder, FormGroup } from '@angular/forms';
import { NavController } from '@ionic/angular';
import { Router } from '@angular/router';
import {AuthService} from '../services/auth.service';

import 'firebase/auth';


@Component({
  selector: 'app-mot-de-passe-oublie',
  templateUrl: './mot-de-passe-oublie.page.html',
  styleUrls: ['./mot-de-passe-oublie.page.scss'],
})
export class MotDePasseOubliePage implements OnInit {

  public control: FormGroup;
  isSubmitted = false;
  constructor(
    public authService: AuthService,
    private formBuilder: FormBuilder
    ) {
      this.control = this.formBuilder.group({
        
        email: ['', [Validators.required, Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$')]]
      });

     

   }

  sendPassword(){
    this.isSubmitted = true;
    this.authService.PasswordRecover(this.control.value['email']);
  }
  get errorControl() {
    return this.control.controls;
  }
  ngOnInit() {
  }

}
